package com.step01.test;

import cn.hutool.core.util.XmlUtil;
import com.lightspring.context.ClassPathXmlApplicationContext;
import com.step01.pojo.UserService;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author: 毅航
 * @create: 2023-10-14 19:32
 * @Description:
 */
public class ApiTest {

    @Test
    public void testClasspathXmlApplicationContext() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        UserService userService = (UserService) context.getBean("userService");
        userService.printMessage("success constructor Context~!~");
    }

    /**
     * @Description: 本地测试读写操作
     * @param
     * @return void
     */

    @Test
    public void readFile() {
        InputStream resourceAsStream = this.getClass().getClassLoader()
                .getResourceAsStream("application.xml");

        //Document doc = XmlUtil.readXML(resourceAsStream);

        if (resourceAsStream != null) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(resourceAsStream))) {
//                String line;
//                while ((line = reader.readLine()) != null) {
//                    System.out.println(line);
//                }
                Document doc = XmlUtil.readXML(reader);
                Element root = doc.getDocumentElement();
                NodeList childNodes = root.getChildNodes();
                for (int i = 0 ; i < childNodes.getLength();  i ++) {
                    // 判断元素
                    if (!(childNodes.item(i) instanceof Element)) {
                        continue;
                    }
                    // 判断对象
                    if (!"bean".equals(childNodes.item(i).getNodeName())) {
                        continue;
                    }
                    Element bean = (Element) childNodes.item(i);
                    String id = bean.getAttribute("id");
                    String name = bean.getAttribute("name");
                    String className = bean.getAttribute("class");


                    System.out.println("id is : "+  id + "class is  : " +  className);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
