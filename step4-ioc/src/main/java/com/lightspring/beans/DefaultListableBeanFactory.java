package com.lightspring.beans;

import com.lightspring.beans.factory.BeanFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: 毅航
 * @create: 2023-10-14 18:48
 * @Description:
 */
public class DefaultListableBeanFactory implements BeanFactory {

    private Map<String, Object> singletons =new HashMap<String,Object>();


    @Override
    public Object getBean(String beanName) throws BeansException {
        return singletons.get(beanName);
    }

    @Override
    public void registerBeanDefinition(String beanName,Object bd) {
        if (this.singletons.containsKey(beanName)) {
            throw new BeansException("bean is not single,You can register multiple bean");
        }
        this.singletons.put(beanName,bd);
    }


}
