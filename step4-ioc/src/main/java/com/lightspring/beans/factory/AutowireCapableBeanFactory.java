package com.lightspring.beans.factory;

import com.lightspring.beans.BeansException;

/**
 *  BeanFactory 接口的扩展实现，它可以实现自动装配，前提是开发者希望为现有的 bean 实例公开此功能。
 * @author 毅航
 * @date 2024/4/5 10:29
 */
public interface AutowireCapableBeanFactory extends BeanFactory {

    <T> T createBean(Class<T> beanClass) throws BeansException;


    /**
     * 实现Bean的自动注入处理
     * @param beanClass
     * @param autowireMode
     * @param dependencyCheck
     * @return
     * @throws BeansException
     */

    Object autowire(Class<?> beanClass, int autowireMode, boolean dependencyCheck) throws BeansException;
}
