package com.lightspring.core.io;

/**
 * 资源加载器
 * @author 毅航
 * @date 2023/11/5 16:02
 */
public interface ResourceLoader {

    public static final String CLASSPATH_URL_PREFIX = "classpath:";

    /**
     * 根据路径获取资源
     * @param location
     * @return
     */
    Resource getResource(String location);

    /**
     * 获取当前资源的类加载器信息
     * @return
     */
    ClassLoader getClassLoader();
}
