package com.step01.test;

import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.XmlUtil;
import com.lightspring.context.ClassPathXmlApplicationContext;
import com.lightspring.core.io.ClassPathResource;
import com.step01.pojo.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author: 毅航
 * @create: 2023-10-14 19:32
 * @Description:
 */
public class ApiTest {

    @Test
    public void testResource() throws IOException {
        ClassPathResource clp = new ClassPathResource("application.xml");
        InputStream resourceAsStream = clp.getInputStream();
        Assert.assertNotNull(resourceAsStream);
    }
}
