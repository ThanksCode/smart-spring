package com.step01.test;

import cn.hutool.core.util.XmlUtil;
import com.lightspring.core.io.DefaultResourceLoader;
import com.lightspring.core.io.Resource;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author: 毅航
 * @create: 2023-10-14 19:32
 * @Description:
 */

public class ApiTest {

   @Test
    public void testDefaultLoader() throws IOException {
       DefaultResourceLoader loader = new DefaultResourceLoader(this.getClass().getClassLoader());
       Resource resource = loader.getResource("classpath:application.xml");
      Assert.assertNotNull(resource);

   }

   @Test
    public void loadingBean() throws IOException {
       DefaultResourceLoader loader = new DefaultResourceLoader(this.getClass().getClassLoader());
       Resource resource = loader.getResource("classpath:application.xml");

      // Document doc = XmlUtil.readXML(resourceAsStream);

       if (resource != null) {
           try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()))) {
//                String line;
//                while ((line = reader.readLine()) != null) {
//                    System.out.println(line);
//                }
               Document doc = XmlUtil.readXML(reader);
               Element root = doc.getDocumentElement();
               NodeList childNodes = root.getChildNodes();
               for (int i = 0 ; i < childNodes.getLength();  i ++) {
                   // 判断元素
                   if (!(childNodes.item(i) instanceof Element)) {
                       continue;
                   }
                   // 判断对象
                   if (!"bean".equals(childNodes.item(i).getNodeName())) {
                       continue;
                   }
                   Element bean = (Element) childNodes.item(i);
                   String id = bean.getAttribute("id");
                   String name = bean.getAttribute("name");
                   String className = bean.getAttribute("class");


                   System.out.println("id is : "+  id + "class is  : " +  className);
               }
           } catch (IOException e) {
               e.printStackTrace();
           }
       }

   }


}
