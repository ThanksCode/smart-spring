package com.lightspring.core.io;

import java.io.IOException;
import java.io.InputStream;


/**
 * 将资源抽象为一个数据流方便后续处理，资源加载的顶层接口
 * @author 毅航
 * @date 2023/11/4 8:06
 */
public interface InputStreamSource {

    InputStream getInputStream() throws IOException;

}
