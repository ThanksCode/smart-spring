package com.lightspring.core.io;

/**
 * 资源加载处理
 * @author 毅航
 * @date 2023/11/4 8:07
 */
public interface Resource extends InputStreamSource{

    /**
     * 是否存在
     * @return
     */
    boolean exists();

    /**
     * 是否可读
     * @return
     */
    default boolean isReadable() {
        return exists();
    }

}
