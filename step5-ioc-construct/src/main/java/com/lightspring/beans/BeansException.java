package com.lightspring.beans;

/**
 * @author: 毅航
 * @create: 2023-10-14 18:45
 * @Description: 自定义Bean异常信息
 */
public class BeansException  extends RuntimeException{

    public BeansException(String msg) {
        super(msg);
    }

    public BeansException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
