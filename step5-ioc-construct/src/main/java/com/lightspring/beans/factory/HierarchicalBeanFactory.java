package com.lightspring.beans.factory;


/**
 *
 * HierarchicalBeanFactory 是 BeanFactory 接口的子接口，它引入了一种分层结构的概念。
 * 这个接口提供了一种方式来管理 bean 工厂之间的父子关系。
 * 在这种层次结构中，每个 HierarchicalBeanFactory 可以有一个父工厂，同时也可以作为其他工厂的父工厂。
 * @author: yihang
 * @description: 具有继承层级结构的bean
 * @date: 22/11/5 19:14
 * @parms
 * @return
 */
public interface HierarchicalBeanFactory extends BeanFactory {

    /**
     * 获取父容器信息
     * @return
     */
    BeanFactory getParentBeanFactory();

    /**
     * 从当前容器获取Bean信息
     * @param name
     * @return
     */
    boolean containsLocalBean(String name);



    ClassLoader getBeanClassLoader();
}
