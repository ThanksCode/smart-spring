package com.lightspring.beans.factory.config;

/**
 * 实现单例bean的注册和加载
 * @author 毅航
 * @date 2024/5/19 22:46
 */
public interface SingletonBeanRegistry {


  /**
   * 获取单利Bean是实例信息
   * @author 毅航
   * @date 2024/5/19 22:47
   */
    Object getSingleton(String beanName);

    /**
     * 向IOC容器中注入Bean实例信息
     * @param beanName
     * @param singletonObject
     */
    void registerSingleton(String beanName, Object singletonObject);
}
