package com.lightspring.beans.factory;

/**
 * @author: yihang
 * @description: factoryBean工厂
 * @date: 22/11/5 20:57
 * @parms
 * @return
 */
public interface FactoryBean<T> {

    /**
     * 返回一个bean信息
     * @author 毅航
     * @date 2024/7/7 18:29
     */
    T getObject() throws Exception;

    Class<?> getObjectType();

    /**
     *
     * @author 毅航
     * @date 2024/7/7 18:29
     */
    boolean isSingleton();

}