package com.lightspring.beans.factory;

import com.lightspring.beans.BeansException;

    /**
 * @author: 毅航
 * @create: 2023-10-14 18:44
 * @Description: Bean工厂
 */
public interface BeanFactory {

    /**
     * 获取一个bean信息
     * @param beanName bean相关表示信息
     * @return result 实例化对象
     * @throws BeansException bean不存在
     */
    Object getBean(String beanName) throws BeansException;

    Object getBean(String name, Object... args) throws BeansException;


    <T> T getBean(String name, Class<T> requiredType) throws BeansException;

    /**
     * @Description: 注册bean信息
     * @param beanName
     * @param bd
     * @return void
     */
    void registerBeanDefinition(String beanName,Object bd);

}
