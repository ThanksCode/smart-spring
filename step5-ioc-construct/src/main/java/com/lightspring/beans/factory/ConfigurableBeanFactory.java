package com.lightspring.beans.factory;

import com.lightspring.beans.factory.config.SingletonBeanRegistry;

/**
 * 提供可配置BeanFactory接口,其可由大多数bean工厂实现。
 * 以提供配置beanFactory工厂配置化手段
 * @author 毅航
 * @date 2024/4/5 10:32
 */
public interface ConfigurableBeanFactory  extends HierarchicalBeanFactory, SingletonBeanRegistry {


    String SCOPE_SINGLETON = "singleton";

    String SCOPE_PROTOTYPE = "prototype";


}
