package com.lightspring.beans.factory.support;

import com.lightspring.beans.factory.config.SingletonBeanRegistry;

import java.util.HashMap;
import java.util.Map;

public class DefaultSingletonBeanRegistry  implements SingletonBeanRegistry {

    /**
     * 默认定义一个Null_object信息
     * */
    protected static final Object NULL_OBJECT = new Object();
    /**
     * 缓存信息，保存bean内容
     */
    private Map<String, Object> singletonObjects = new HashMap<>(16);


    /**
     * 基础版：从singletonObjects中获取bean信息
     * @author 毅航
     * @date 2024/7/7 18:23
     */
    @Override
    public Object getSingleton(String beanName) {
        return singletonObjects.get(beanName);
    }

    /**
     * 基础版：将bean信息放入到容器中
     * @author 毅航
     * @date 2024/7/7 18:24
     */
    @Override
    public void registerSingleton(String beanName, Object singletonObject) {
        singletonObjects.put(beanName, singletonObject);
    }
}
