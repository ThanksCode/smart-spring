package com.lightspring.beans.factory;

import com.lightspring.beans.BeansException;

import java.util.Map;

/**
 * BeanFactory 接口的扩展实现，它可以列举出所有 bean 实例，而不是按客户端调用的要求，
 * 按照名称一一进行 bean 的依赖查找。
 * 具有 “预加载其所有 bean 定义信息” 的 BeanFactory 实现（例如基于XML的 BeanFactory ）可以实现此接口。
 * @author 毅航
 * @date 2024/1/7 15:48
 */
public interface ListableBeanFactory  extends BeanFactory {

    /**
     * 按照类型返回 Bean 实例
     * @param type
     * @param <T>
     * @return
     * @throws BeansException
     */
    < T> Map<String, T> getBeansOfType(Class<T> type) throws BeansException;

    /**
     * Return the names of all beans defined in this registry.
     *
     * 返回注册表中所有的Bean名称
     */
    String[] getBeanDefinitionNames();
}
