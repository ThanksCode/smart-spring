package com.lightspring.beans;

/**
 * 记录属性信息将配置信息解析为 name ,value 的键值对信息
 * @author 毅航
 * @date 2024/7/7 19:19
 */
public class PropertyValue {

    private final String name;

    private final Object value;

    public PropertyValue(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }
}
